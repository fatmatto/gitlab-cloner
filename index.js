const argv = require('yargs').argv
const request = require('@fatmatto/ptth')
const Git = require('simple-git/promise')
const fs = require('fs')
const rootGroupId = argv.group
const baseDir = String(argv.dir)
const privateToken = String(argv.token)
const git = Git(baseDir)
const manifest = require('./package.json')
let projectsToClone = []

function printHelp () {
  console.log(`Usage:

node index.js --dir=<path/to/dir> --group=<groupId> --token=<token>  `)
}

if (!baseDir || !privateToken || !rootGroupId) {
  printHelp()
  process.exit(1)
}

function getProjectsInGroup (groupId) {
  return request({
    method: 'GET',
    url: `https://gitlab.com/api/v4/groups/${groupId}/projects`,
    query: {
      per_page: 100
    },
    headers: {
      'Private-Token': privateToken
    }
  })
}

function getSubGroups (groupId) {
  return request({
    method: 'GET',
    url: `https://gitlab.com/api/v4/groups/${groupId}/subgroups`,
    query: {
      per_page: 100
    },
    headers: {
      'Private-Token': privateToken
    }
  })
}

async function loadGroup (id, name) {
  const label = name || id
  console.log('=================================================================')
  console.log('Fetching group ' + label + ' :')
  let res = await getSubGroups(id)
  const groups = res.body
  res = await getProjectsInGroup(id)
  const projects = res.body
  projectsToClone = projectsToClone.concat(projects)
  console.log(`${projects.length} projects found`)
  console.log(`${groups.length} sub-groups found`)
  console.log(`Partial number of projects in org: ${projectsToClone.length}`)
  console.log('==================================================================')
  for (let group of groups) {
    await loadGroup(group.id)
  }
}

async function main () {
  console.log('\n')
  console.log('       *.                  *.')
  console.log('      ***                 ***')
  console.log('     *****               *****')
  console.log('    .******             *******')
  console.log('    ********            ********')
  console.log('   ,,,,,,,,,***********,,,,,,,,,')
  console.log('  ,,,,,,,,,,,*********,,,,,,,,,,,')
  console.log('  .,,,,,,,,,,,*******,,,,,,,,,,,,')
  console.log('      ,,,,,,,,,*****,,,,,,,,,.')
  console.log('         ,,,,,,,****,,,,,,')
  console.log('            .,,,***,,,,')
  console.log('                ,*,.')
  console.log('\n')
  console.log(`\nGitlab Group Cloner v${manifest.version}\n`)
  if (!fs.existsSync(baseDir)) {
    console.log(`Cannot clone group into ${baseDir}, directory does not exist or is not writeable`)
    process.exit(1)
  }
  console.log('Fetching data from Gitlab Api for group ' + rootGroupId)
  await loadGroup(rootGroupId)
  console.log('Done fetching data from Gitlab Api!')
  console.log(`Cloning ${projectsToClone.length} projects into ${baseDir}`)

  for (let repo of projectsToClone) {
    console.log(`Cloning ${repo.name} ...`)
    await git.silent(false).clone(repo.http_url_to_repo)
    console.log('Done!')
  }
}

main()
