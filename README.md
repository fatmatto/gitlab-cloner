### gitlab-cloner
Clone all your repositories under a group

### Usage

```bash
git clone https://gitlab.com/fatmatto/gitlab-cloner.git
cd gitlab-cloner
npm i
node index.js --dir=<path/to/dir> --group=<groupId> --token=<token>
```